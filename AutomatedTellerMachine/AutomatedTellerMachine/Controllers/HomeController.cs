﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutomatedTellerMachine.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult TellerMachine()
        {
            return View("TellerMachine");
        }

        //GET/Home/Index
        [MyLoggingFilter]
        public ActionResult Index()
        {
            return View();
        }

        //GET/Home/Index
        [ActionName("about-this-atm")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About");
        }

        public ActionResult Contact()
        {

         ViewBag.theMessage = "Having trouble? Send us a message.";

         return View();
        }

        [HttpPost]
        public ActionResult Contact(string message)
        {
            //TODO: send message to HQ
            ViewBag.theMessage = "We got your message!";

            return View();
        }

        public ActionResult Foo()
        {
            ViewBag.Message("I'm creating new function in app controller.");
            return View("About");
        }

        public ActionResult serial(string letterCase)
        {
            var serial = "ASPNETMVC5ATM1";
            if (letterCase == "lower")
            {
                return Content(serial.ToLower());
            }
            //return Content(serial);
            //return new HttpStatusCodeResult(403);
            return Json(new { name = "serial", value =serial }, JsonRequestBehavior.AllowGet);
        }
        /*
         * Attribute routing
         * [Route("Home/Create)]
         * public ActionResult Create()
         * {
         *  db.Add(student{name=Adam , degree=b});
         *  db.SaveChanges();
         *  return View();
         * }
         */ 

         /* //autorization filter
          * [Autorize(Roles="administrator" , Users="jsmith"]
          * [HttpPost]
          * public ActionResult Create(Customer customer)
          * {
          *  db.customers.Add(customer);
          *  db.SaveChanges();
          *  
          *  return RedirectToAction("Index");
          *  }
          *  //or
          *  [Autorize]//allow any loged-in user
          *  [AllowAnonymous]//exception in "autorized" controller.
          * 
          */
    }
}